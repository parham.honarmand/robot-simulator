package view;

import controller.Controller;
import exception.ValidationException;
import model.enums.Side;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class View {
    static List<String> commands = new ArrayList<>();
    static Controller controller;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        controller = new Controller();

        System.out.println("Welcome to robot simulator");
        System.out.println("Please enter your commands :");
        while (true) {
            String input = scanner.nextLine();
            commands.add(input);
            if (input.equals("REPORT"))
                break;
        }

        processCommands();
    }

    private static void processCommands() {
        for (String command : commands) {
            String[] com = command.split(" ");
            switch (com[0]) {
                case "MOVE":
                    move();
                    break;
                case "PLACE":
                    place(command);
                    break;
                case "REPORT":
                    report();
                    break;
                case "LEFT":
                    changeDirection(Side.LEFT);
                    break;
                case "RIGHT":
                    changeDirection(Side.RIGHT);
                    break;
                default:
                    System.out.println(String.format("Bad command : %S", command));
            }
        }
    }

    private static void report() {
        try {
            System.out.println(controller.report());
        } catch (ValidationException e) {
            System.out.println(e.getMessage());
        }
    }

    private static void changeDirection(Side side) {
        try {
            controller.changeDirection(side);
        } catch (ValidationException e) {
            System.out.println(e.getMessage());
        }
    }

    private static void move() {
        try {
            controller.move();
        } catch (ValidationException e) {
            System.out.println(e.getMessage());
        }
    }

    private static void place(String command) {
        String[] input = command.split(" ")[1].split(",");
        if (input.length < 3)
            System.out.println(String.format("Bad command : %S", command));

        try {
            controller.place(Integer.parseInt(input[0]), Integer.parseInt(input[1]), input[2]);
        } catch (ValidationException e) {
            System.out.println(e.getMessage());
        }
    }
}
