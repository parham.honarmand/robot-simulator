package controller;

import exception.ValidationException;
import model.Robot;
import model.enums.Direction;
import model.enums.Side;

public class Controller {
    private final int boardLength = 5;
    private Robot robot;

    public Controller() {
        robot = new Robot(boardLength);
    }

    public void place(int x, int y, String dir) throws ValidationException {
        Direction direction;
        switch (dir) {
            case "EAST":
                direction = Direction.EAST;
                break;
            case "WEST":
                direction = Direction.WEST;
                break;
            case "NORTH":
                direction = Direction.NORTH;
                break;
            case "SOUTH":
                direction = Direction.SOUTH;
                break;
            default:
                throw new ValidationException(String.format("Not a valid direction : %s", dir));
        }
        robot.place(x, y, direction);
    }

    public void move() throws ValidationException {
        robot.move();
    }

    public void changeDirection(Side side) throws ValidationException {
        robot.changeDirection(side);
    }

    public String report() throws ValidationException {
        return robot.report();
    }
}
