package exception;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class ValidationException extends Exception {
    public ValidationException(String message) {
        super(message);
    }
}
