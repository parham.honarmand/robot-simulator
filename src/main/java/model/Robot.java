package model;

import exception.ValidationException;
import model.enums.Direction;
import model.enums.Side;

public class Robot {
    private Direction direction;
    private Position position;
    private Integer boardLength;
    private boolean isPlaced;

    public Robot(int boardLength) {
        this.boardLength = boardLength;
    }

    public void move() throws ValidationException {
        if (!isPlaced)
            throw new ValidationException("First, place robot");
        switch (direction) {
            case EAST:
                moveEast();
                break;
            case WEST:
                moveWest();
                break;
            case NORTH:
                moveNorth();
                break;
            case SOUTH:
                moveSouth();
                break;
        }
    }

    private void moveEast() throws ValidationException {
        if (position.getX() < boardLength - 1)
            position.incrementX();
        else
            throw new ValidationException("Can not move to East");
    }

    private void moveWest() throws ValidationException {
        if (position.getX() > 0)
            position.decrementX();
        else
            throw new ValidationException("Can not move to West");
    }

    private void moveNorth() throws ValidationException {
        if (position.getY() < boardLength - 1)
            position.incrementY();
        else
            throw new ValidationException("Can not move to North");
    }

    private void moveSouth() throws ValidationException {
        if (position.getY() > 0)
            position.decrementY();
        else
            throw new ValidationException("Can not move to South");
    }

    public void place(int x, int y, Direction direction) throws ValidationException {
        Position position = new Position(x, y);
        isValidPosition(position);
        this.position = position;
        this.direction = direction;
        isPlaced = true;
    }

    private void isValidPosition(Position position) throws ValidationException {
        if (position.getY() < 0 || position.getY() > boardLength - 1
                || position.getX() < 0 || position.getX() > boardLength - 1)
            throw new ValidationException("Not a valid position");
    }

    public void changeDirection(Side side) throws ValidationException {
        if (!isPlaced)
            throw new ValidationException("First place robot");
        switch (side) {
            case LEFT:
                changeToLeft();
                break;
            case RIGHT:
                changeToRight();
                break;
        }
    }

    private void changeToLeft() {
        switch (direction) {
            case SOUTH:
                direction = Direction.EAST;
                break;
            case NORTH:
                direction = Direction.WEST;
                break;
            case WEST:
                direction = Direction.SOUTH;
                break;
            case EAST:
                direction = Direction.NORTH;
                break;
        }
    }

    private void changeToRight() {
        switch (direction) {
            case SOUTH:
                direction = Direction.WEST;
                break;
            case NORTH:
                direction = Direction.EAST;
                break;
            case WEST:
                direction = Direction.NORTH;
                break;
            case EAST:
                direction = Direction.SOUTH;
                break;
        }
    }

    public String report() throws ValidationException {
        if (!isPlaced)
            throw new ValidationException("First place robot");
        return String.format("%d, %d, %s", position.getX(), position.getY(), direction);
    }
}
