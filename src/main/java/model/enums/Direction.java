package model.enums;

public enum Direction {
    WEST, EAST, NORTH, SOUTH
}
