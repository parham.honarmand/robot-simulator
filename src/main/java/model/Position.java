package model;

import lombok.*;

@AllArgsConstructor
@Getter
public class Position {
    private int x;
    private int y;

    public void incrementX() {
        x++;
    }

    public void decrementX() {
        x--;
    }

    public void incrementY() {
        y++;
    }

    public void decrementY() {
        y--;
    }
}
