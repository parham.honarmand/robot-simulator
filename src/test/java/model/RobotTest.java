package model;

import exception.ValidationException;
import model.enums.Direction;
import model.enums.Side;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.apache.commons.lang3.RandomUtils;

import static org.junit.jupiter.api.Assertions.*;

class RobotTest {
    Robot robot;

    @BeforeEach
    private void before() {
        robot = new Robot(5);
    }

    @Nested
    @DisplayName("Move tests")
    class Move {
        @Test
        @DisplayName("Move without placed robot")
        void moveWithOutPlaced() {
            //given
            //when
            Exception actual = assertThrows(
                    Exception.class,
                    () -> robot.move());

            Exception expected = new ValidationException("First, place robot");
            //then
            assertEquals("First, place robot", actual.getMessage());
            assertEquals(expected, actual);
        }

        @Test
        @DisplayName("Cant move to East")
        void cantMoveToEast() throws ValidationException {
            //given
            robot.place(4, RandomUtils.nextInt(0, 5), Direction.EAST);
            //when
            Exception actual = assertThrows(
                    Exception.class,
                    () -> robot.move());

            Exception expected = new ValidationException("Can not move to East");
            //then
            assertEquals("Can not move to East", actual.getMessage());
            assertEquals(expected, actual);
        }

        @Test
        @DisplayName("Cant move to West")
        void cantMoveToWest() throws ValidationException {
            //given
            robot.place(0, RandomUtils.nextInt(0, 5), Direction.WEST);
            //when
            Exception actual = assertThrows(
                    Exception.class,
                    () -> robot.move());

            Exception expected = new ValidationException("Can not move to West");
            //then
            assertEquals("Can not move to West", actual.getMessage());
            assertEquals(expected, actual);
        }

        @Test
        @DisplayName("Cant move to North")
        void cantMoveToNorth() throws ValidationException {
            //given
            robot.place(RandomUtils.nextInt(0, 5), 4, Direction.NORTH);
            //when
            Exception actual = assertThrows(
                    Exception.class,
                    () -> robot.move());

            Exception expected = new ValidationException("Can not move to North");
            //then
            assertEquals("Can not move to North", actual.getMessage());
            assertEquals(expected, actual);
        }

        @Test
        @DisplayName("Cant move to South")
        void cantMoveToSouth() throws ValidationException {
            //given
            robot.place(RandomUtils.nextInt(0, 5), 0, Direction.SOUTH);
            //when
            Exception actual = assertThrows(
                    Exception.class,
                    () -> robot.move());

            Exception expected = new ValidationException("Can not move to South");
            //then
            assertEquals("Can not move to South", actual.getMessage());
            assertEquals(expected, actual);
        }

        @Test
        @DisplayName("Move to South")
        void MoveToSouth() throws ValidationException {
            //given
            int i = RandomUtils.nextInt(0, 5);
            robot.place(i, 2, Direction.SOUTH);
            //when
            robot.move();
            String actual = robot.report();
            String expected = String.format("%d, 1, SOUTH", i);
            //then
            assertEquals(expected, actual);
        }

        @Test
        @DisplayName("Move to North")
        void MoveToNorth() throws ValidationException {
            //given
            int i = RandomUtils.nextInt(0, 5);
            robot.place(i, 2, Direction.NORTH);
            //when
            robot.move();
            String actual = robot.report();
            String expected = String.format("%d, 3, NORTH", i);
            //then
            assertEquals(expected, actual);
        }

        @Test
        @DisplayName("Move to West")
        void MoveToWest() throws ValidationException {
            //given
            int i = RandomUtils.nextInt(0, 5);
            robot.place(2, i, Direction.WEST);
            //when
            robot.move();
            String actual = robot.report();
            String expected = String.format("1, %d, WEST", i);
            //then
            assertEquals(expected, actual);
        }

        @Test
        @DisplayName("Move to East")
        void MoveToEast() throws ValidationException {
            //given
            int i = RandomUtils.nextInt(0, 5);
            robot.place(2, i, Direction.EAST);
            //when
            robot.move();
            String actual = robot.report();
            String expected = String.format("3, %d, EAST", i);
            //then
            assertEquals(expected, actual);
        }
    }

    @Nested
    @DisplayName("Place tests")
    class Place {
        @Test
        @DisplayName("Place successfully")
        void place() throws ValidationException {
            //given
            int x = RandomUtils.nextInt(0, 5);
            int y = RandomUtils.nextInt(0, 5);
            //when
            robot.place(x, y, Direction.EAST);
            String actual = robot.report();
            String expected = String.format("%d, %d, EAST", x, y);
            //then
            assertEquals(expected, actual);
        }

        @Test
        @DisplayName("Place with invalid x")
        void placeWithInvalidX() {
            //given
            int y = RandomUtils.nextInt(0, 5);
            //when
            Exception actual = assertThrows(
                    Exception.class,
                    () -> robot.place(6, y, Direction.EAST));

            Exception expected = new ValidationException("Not a valid position");
            //then
            assertEquals("Not a valid position", actual.getMessage());
            assertEquals(expected, actual);
        }

        @Test
        @DisplayName("Place with invalid y")
        void placeWithInvalidY() {
            //given
            int x = RandomUtils.nextInt(0, 5);
            //when
            Exception actual = assertThrows(
                    Exception.class,
                    () -> robot.place(x, 6, Direction.EAST));

            Exception expected = new ValidationException("Not a valid position");
            //then
            assertEquals("Not a valid position", actual.getMessage());
            assertEquals(expected, actual);
        }
    }


    @Nested
    @DisplayName("Change direction tests")
    class ChangeDirection {
        @Test
        @DisplayName("Change direction without placed robot")
        void changeDirectionWithoutPlaced() {
            //given
            //when
            Exception actual = assertThrows(
                    Exception.class,
                    () -> robot.changeDirection(Side.RIGHT));
            Exception expected = new ValidationException("First place robot");
            //then
            assertEquals("First place robot", actual.getMessage());
            assertEquals(expected, actual);
        }

        @Test
        @DisplayName("Change direction from West to right")
        void changeDirectionFromWestToRight() throws ValidationException {
            //given
            int x = RandomUtils.nextInt(0, 5);
            int y = RandomUtils.nextInt(0, 5);
            robot.place(x, y, Direction.WEST);
            //when
            robot.changeDirection(Side.RIGHT);
            String actual = robot.report();
            String expected = String.format("%d, %d, NORTH", x, y);
            //then
            assertEquals(expected, actual);
        }

        @Test
        @DisplayName("Change direction from West to left")
        void changeDirectionFromWestToLeft() throws ValidationException {
            //given
            int x = RandomUtils.nextInt(0, 5);
            int y = RandomUtils.nextInt(0, 5);
            robot.place(x, y, Direction.WEST);
            //when
            robot.changeDirection(Side.LEFT);
            String actual = robot.report();
            String expected = String.format("%d, %d, SOUTH", x, y);
            //then
            assertEquals(expected, actual);
        }

        @Test
        @DisplayName("Change direction from East to left")
        void changeDirectionFromEastToLeft() throws ValidationException {
            //given
            int x = RandomUtils.nextInt(0, 5);
            int y = RandomUtils.nextInt(0, 5);
            robot.place(x, y, Direction.EAST);
            //when
            robot.changeDirection(Side.LEFT);
            String actual = robot.report();
            String expected = String.format("%d, %d, NORTH", x, y);
            //then
            assertEquals(expected, actual);
        }

        @Test
        @DisplayName("Change direction from East to right")
        void changeDirectionFromEastToRight() throws ValidationException {
            //given
            int x = RandomUtils.nextInt(0, 5);
            int y = RandomUtils.nextInt(0, 5);
            robot.place(x, y, Direction.EAST);
            //when
            robot.changeDirection(Side.RIGHT);
            String actual = robot.report();
            String expected = String.format("%d, %d, SOUTH", x, y);
            //then
            assertEquals(expected, actual);
        }

        @Test
        @DisplayName("Change direction from North to left")
        void changeDirectionFromNorthToLeft() throws ValidationException {
            //given
            int x = RandomUtils.nextInt(0, 5);
            int y = RandomUtils.nextInt(0, 5);
            robot.place(x, y, Direction.NORTH);
            //when
            robot.changeDirection(Side.LEFT);
            String actual = robot.report();
            String expected = String.format("%d, %d, WEST", x, y);
            //then
            assertEquals(expected, actual);
        }

        @Test
        @DisplayName("Change direction from North to right")
        void changeDirectionFromNorthToRight() throws ValidationException {
            //given
            int x = RandomUtils.nextInt(0, 5);
            int y = RandomUtils.nextInt(0, 5);
            robot.place(x, y, Direction.NORTH);
            //when
            robot.changeDirection(Side.RIGHT);
            String actual = robot.report();
            String expected = String.format("%d, %d, EAST", x, y);
            //then
            assertEquals(expected, actual);
        }

        @Test
        @DisplayName("Change direction from South to left")
        void changeDirectionFromSouthToLeft() throws ValidationException {
            //given
            int x = RandomUtils.nextInt(0, 5);
            int y = RandomUtils.nextInt(0, 5);
            robot.place(x, y, Direction.SOUTH);
            //when
            robot.changeDirection(Side.LEFT);
            String actual = robot.report();
            String expected = String.format("%d, %d, EAST", x, y);
            //then
            assertEquals(expected, actual);
        }

        @Test
        @DisplayName("Change direction from South to right")
        void changeDirectionFromSouthToRight() throws ValidationException {
            //given
            int x = RandomUtils.nextInt(0, 5);
            int y = RandomUtils.nextInt(0, 5);
            robot.place(x, y, Direction.SOUTH);
            //when
            robot.changeDirection(Side.RIGHT);
            String actual = robot.report();
            String expected = String.format("%d, %d, WEST", x, y);
            //then
            assertEquals(expected, actual);
        }
    }

    @Nested
    @DisplayName("Report tests")
    class Report {
        @Test
        @DisplayName("Report without placed robot")
        void reportWithoutPlacedRobot() {
            //given
            //when
            Exception actual = assertThrows(
                    Exception.class,
                    () -> robot.report());

            Exception expected = new ValidationException("First place robot");
            //then
            assertEquals("First place robot", actual.getMessage());
            assertEquals(expected, actual);
        }

        @Test
        @DisplayName("Report successfully")
        void reportSuccessfully() throws ValidationException {
            //given
            int x = RandomUtils.nextInt(0, 5);
            int y = RandomUtils.nextInt(0, 5);
            robot.place(x, y, Direction.EAST);
            //when
            String actual = robot.report();
            String expected = String.format("%d, %d, EAST", x, y);
            //then
            assertEquals(expected, actual);
        }
    }
}