package model;

import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PositionTest {
    Position position;
    int x;
    int y;

    @BeforeEach
    void before() {
        x = RandomUtils.nextInt();
        y = RandomUtils.nextInt();
        position = new Position(x, y);
    }

    @Test
    @DisplayName("Increment X")
    void incrementX() {
        //given
        //when
        position.incrementX();
        int actual = position.getX();
        int expected = ++x;
        //then
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Decrement X")
    void decrementX() {
        //given
        //when
        position.decrementX();
        int actual = position.getX();
        int expected = --x;
        //then
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Increment Y")
    void incrementY() {
        //given
        //when
        position.incrementY();
        int actual = position.getY();
        int expected = ++y;
        //then
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Decrement Y")
    void decrementY() {
        //given
        //when
        position.decrementY();
        int actual = position.getY();
        int expected = --y;
        //then
        assertEquals(expected, actual);
    }
}